import React from "react";
import { Helmet } from "react-helmet"
import Header from "./header";
import Carousel from "./Carousel/carousel"
import ScreenSizeProvider from "../Providers/ScreenProvider"

const Homepage = () => {

  return (
    <ScreenSizeProvider>
      <Helmet>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato"/>
      </Helmet>
      <Header />
      <Carousel/>
      <div>PICTURE TWO</div>
      <div>PARAGRAPH</div>
    </ScreenSizeProvider>
  );
};

export default Homepage;