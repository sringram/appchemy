import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { useState } from "react";

const Header = ({ siteTitle }) => {
  const [logIn, setLogInStatus] = useState(false);

  return (
    <header
      className="alchemyHeaderContainer"
    >
      <div className="alchemyHeaderContent">
        <img
          className="alchemyMMAHeaderLogo"
          alt="alchemy mma logo"
          src={`https://alchemymma.net/images/logos/alchemy-words.png`}
        />
        <div className="loginDiv">
          {logIn ? (
            <Link to="../pageLayouts/Login">Account</Link>
          ) : (
            <p
              className="headerLogInCallout"
            >
              You are not Logged In!,
              <Link
                className="LogInLink"
                to="../pageLayout/Login"
              >
                {`  Log In`}
              </Link>
            </p>
          )}
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
