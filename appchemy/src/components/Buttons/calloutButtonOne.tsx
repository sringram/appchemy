import React, { useState, useEffect } from "react";
import { useWindowSize } from "../../Providers/ScreenProvider";

const CalloutButtonOne = ({ calloutText }) => {
  const { width, height } = useWindowSize();
  const Desktop = {
    name: "DesktopCalloutButtonOne",
    height: "auto",
    width: "12vw",
    backgroundColor: "#ec910e",
    borderRadius: "30px",
    textTransform: "uppercase",
    padding: ".6vw",
    color: "#000",
    fontWeight: 800,
    letterSpacing: "1px",
    marginTop: "1vh",
    textAlign: "center",
    fontSize: "1vw",
    fontFamily: "Lato !important",
    position: "absolute",
    float: "left",
    left: "32%",
    boxSizing: "border-box"
  };
  const Tablet = {
    name: "TabletCalloutButtonOne",
    height: "auto",
    width: "12vw",
    backgroundColor: "#ec910e",
    borderRadius: "30px",
    textTransform: "uppercase",
    padding: "0vw",
    color: "#000",
    fontWeight: 800,
    letterSpacing: "1px",
    marginTop: "auto",
    textAlign: "center",
    fontSize: "1vw",
    fontFamily: "Lato !important",
    position: "absolute",
    float: "left",
    left: "32%",
    boxSizing: "border-box"
  };
  const Mobile = {
    name: "MobileCalloutButtonOne",
    height: "auto",
    width: "12vw",
    backgroundColor: "#ec910e",
    borderRadius: "30px",
    textTransform: "uppercase",
    padding: "0vw",
    color: "#000",
    fontWeight: 800,
    letterSpacing: "1px",
    marginTop: "0px",
    textAlign: "center",
    fontSize: "1vw",
    fontFamily: "Lato !important",
    position: "absolute",
    float: "left",
    left: "32%",
    boxSizing: "border-box"
  };

  let originalState=Desktop;
  if (width > 500 && width <= 1000) {
    originalState=Tablet;
  } else if (width <= 500) {
    originalState=Mobile;
  }
  const [cssStyles, setCssStyles] = useState(originalState);

  useEffect(() => {
    if (width > 1000) {
      setCssStyles(Desktop);
    } else if (width > 500 && width <= 1000) {
      setCssStyles(Tablet);
    } else {
      setCssStyles(Mobile);
    }
  }, [width]);
  return (
    <div key="calloutButtonOneDiv" className={cssStyles.name} style={{
        height: `${cssStyles.height}`,
        width: `${cssStyles.width}`,
        backgroundColor: `${cssStyles.backgroundColor}`,
        borderRadius: `${cssStyles.borderRadius}`,
        textTransform: `${cssStyles.textTransform}`,
        padding: `${cssStyles.padding}`,
        fontWeight: cssStyles.fontWeight,
        letterSpacing: `${cssStyles.letterSpacing}`,
        marginTop:`${cssStyles.marginTop}`,
        textAlign: `${cssStyles.textAlign}`,
        fontSize: `${cssStyles.fontSize}`,
        fontFamily: "Lato !important",
        position: cssStyles.position,
        float: `${cssStyles.float}`,
        left: `${cssStyles.left}`,
        boxSizing: `${cssStyles.boxSizing}`
        }}>
      {width > 800 ? calloutText: "Enroll"}
    </div>
  );
};

export default CalloutButtonOne;
