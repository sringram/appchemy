import React, { useEffect, useState } from "react";
import CalloutButtonOne from "../Buttons/calloutButtonOne"
import { useWindowSize } from "../../Providers/ScreenProvider"

const CarouselCaption = ({ titeText, callout1, callout2, callout3 }) => {
  const { width,height } = useWindowSize();
  const Desktop = { 
    name: 'DesktopCaption',
    position:'relative',
    width: '31vw',
    height: '40vh',
    right: '10%',
    top: '19vh',
    float: "right",
    h1: { color: '#fff', fontFamily: "Lato !important", textShadow: '0 2px 0 #000', textAlign: 'center', lineHeight: 1.2, letterSpacing: "-.2px", marginBottom: '1vh', fontSize: '2.2vw'},
    h3: { color: '#fff', fontFamily: "Lato !important", fontSize: '1.7vw', fontWeight: 600, textAlign: 'center', marginBottom: '1vh'},
    h5: { color: '#fff', fontFamily: "Lato !important", fontSize: '1.3vw', fontWeight: 400, textAlign: 'center', marginBottom: '1vh'},
    p: { color: '#fff', fontFamily: "Lato !important", fontSize: '1vw', fontWeight: 400, textAlign: 'center', marginBottom: '1vh'}
  }
  const Tablet = { 
    name: 'TabletCaption',
    position:'relative',
    width: '31vw',
    height: '20vh',
    right: '13%',
    top: '11vh',
    float: "right",
    h1: { color: '#fff', fontFamily: "Lato !important", textShadow: '0 2px 0 #000', textAlign: 'center', lineHeight: 1.2, letterSpacing: "-.2px", marginBottom: '1vh', fontSize: '2.2vw'},
    h3: { color: '#fff', fontFamily: "Lato !important", fontSize: '1.7vw', fontWeight: 600, textAlign: 'center', marginBottom: '10px'},
    h5: { color: '#fff', fontFamily: "Lato !important", fontSize: '1.3vw', fontWeight: 400, textAlign: 'center', marginBottom: '10px'},
    p: { color: '#fff', fontFamily: "Lato !important", fontSize: '1vw', fontWeight: 400, textAlign: 'center', marginBottom: '10px'}
  }
  const Mobile = { 
    name: 'MobileCaption',
    position:'relative',
    width: '31vw',
    height: '10vh',
    right: '13%',
    top: '5vh',
    float: "right",
    h1: { color: '#fff', fontFamily: "Lato !important", textShadow: '0 2px 0 #000', textAlign: 'center', lineHeight: 1.2, letterSpacing: "-.2px", marginBottom: '1vh', fontSize: '2.2vw'},
    h3: { color: '#fff', fontFamily: "Lato !important", fontSize: '1.7vw', fontWeight: 600, textAlign: 'center', marginBottom: '10px'},
    h5: { color: '#fff', fontFamily: "Lato !important", fontSize: '1.3vw', fontWeight: 400, textAlign: 'center', marginBottom: '10px'},
    p: { color: '#fff', fontFamily: "Lato !important", fontSize: '1vw', fontWeight: 400, textAlign: 'center', marginBottom: '10px'}
  }

  let originalState = Desktop
  if(width > 500 && width <= 1000){
    originalState = Tablet

  }
  else if (width <= 500) {
    originalState = Mobile
  }
  const [cssStyles, setCssStyles] = useState(originalState);
  const [showSmalltext, setShowSmallText] = useState(width > 800)

 

  useEffect(() => {
      if(width > 1000){
        setCssStyles(Desktop)
        setShowSmallText(true)
      }
      else if(width > 500 && width <= 1000){
        setCssStyles(Tablet)
        setShowSmallText(false)

      }
      else {
        setCssStyles(Mobile)
        setShowSmallText(false)
      }
  }, [width])
  return (
    <div className={`${cssStyles.name}`}
        style={{
        position: `${cssStyles.position}`,
        width: `${cssStyles.width}`,
        height: `${cssStyles.height}`,
        right: `${cssStyles.right}`,
        top: `${cssStyles.top}`,
        float: `${cssStyles.float}`,
    }}>
      {titeText && (
        <h1 style={{ color: `${cssStyles.h1.color}`, fontFamily: "Lato !important", textShadow: `${cssStyles.h1.textShadow}`, textAlign: `${cssStyles.h1.textAlign}`, lineHeight: `${cssStyles.h1.lineHeight}`, letterSpacing: `${cssStyles.h1.letterSpacing}`, marginBottom: `${cssStyles.h1.marginBottom}`, fontSize: `${cssStyles.h1.fontSize}`}}>
          {titeText}
        </h1>
      )}
      {callout1 && <h3 style={{ color: '#fff', fontFamily: "Lato !important", fontSize: `${cssStyles.h3.fontSize}`, fontWeight: 600, textAlign: 'center', marginBottom: `${cssStyles.h3.marginBottom}`}}>{callout1}</h3>}
      {callout2 && showSmalltext && <h5 style={{ color: '#fff', fontFamily: "Lato !important", fontSize: `${cssStyles.h5.fontSize}`, fontWeight: 500, textAlign: 'center', marginBottom: `${cssStyles.h5.marginBottom}` }}>{callout2}</h5>}
      {callout3 && showSmalltext && <p style={{ color: '#fff', fontFamily: "Lato !important", fontSize: `${cssStyles.p.fontSize}`, fontWeight: 400, textAlign: 'center', marginBottom: `${cssStyles.p.marginBottom}`}}>{callout3}</p>}
      <CalloutButtonOne calloutText={"Enroll Now"}/>
      </div>
  );
};

export default CarouselCaption;
