import React from "react";

const CarouselItem = props => {
    return(
    <div className='carouselItem'
    style={{
        width: `${props.width}px`,
        transform: `translateX(-${props.translate}px)`,
        transition: `transform ease-out ${props.transition}s`,
        height: `100%`,
        display: `flex`,
    }}>{props.children}</div>
    )
};

export default CarouselItem;