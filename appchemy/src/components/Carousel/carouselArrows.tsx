import React from "react";

const CarouselArrows = props => {
  return (
    <div
      onClick={props.handleClick}
      className={`the${props.direction}one`}
      style={{
        background: `none`,
        position: "absolute",
        height: "100%",
        width: "3%",
        top: '0px',
        right: `${props.direction === "right" ? `0px` : `na`}`,
        left: `${props.direction === "left" ? '0px' : `na`}`,
        alignItems: 'right'
      }}
    >
    { props.direction === "right" ?
      <div
        style={{
          opacity: "1",
          display: `flex`,
          position: `absolute`,
          top: `50%`,
          right: '40%',
          height: `50px`,
          width: `50px`,
          justifyContent: `center`,
          background: `white`,
          borderRadius: `50%`,
          cursor: `pointer`,
          alignItems: `center`,
          transition: `transform ease-in 0.1s`,
          // hover: {
          // transform: scale(1.1)
          // },
          // img: {
          //     transform: translateX(`${direction === "left" ? "-2" : "2"}px`),
          //     focus: {
          //         outline: 0
          //     }
          // }
        }}
      >
          <div className="arrow-right" />
      </div>
      :
      <div
      style={{
        opacity: "1",
        display: `flex`,
        position: `absolute`,
        top: `50%`,
        left: '30%',
        height: `50px`,
        width: `50px`,
        justifyContent: `center`,
        background: `white`,
        borderRadius: `50%`,
        cursor: `pointer`,
        alignItems: `center`,
        transition: `transform ease-in 0.1s`,
        // hover: {
        // transform: scale(1.1)
        // },
        // img: {
        //     transform: translateX(`${direction === "left" ? "-2" : "2"}px`),
        //     focus: {
        //         outline: 0
        //     }
        // }
      }}
    >
        <div className="arrow-left" />
    </div>
    }
    </div>
  );
};

export default CarouselArrows;
