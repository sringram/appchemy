import CarouselItem from "./carouselItem";
import CarouselImage from "./carouselImage";
import CarouselArrows from "./carouselArrows";
import CarouselDots from "./carouselDots";
import { useWindowSize } from "../../Providers/ScreenProvider"
import  data from "./data.json"
import React, { useState, useEffect } from "react";

const Carousel = props => {
  const slideConfigurations = data.slides

  const [state, setState] = useState({
    translate: 0,
    transition: 0.45,
    activeIndex: 0,
  });


  const { width, height } = useWindowSize();
  const { translate, transition, activeIndex} = state;

  const prevSlide = () => {
    if (activeIndex === 0) {
      return setState({
        ...state,
        translate: (slideConfigurations.length - 1) * width,
        activeIndex: slideConfigurations.length - 1,
      });
    }

    setState({
      ...state,
      activeIndex: activeIndex - 1,
      translate: (activeIndex - 1) * width,
    });
  };

  const nextSlide = () => {
    if (activeIndex === slideConfigurations.length - 1) {
      return setState({
        ...state,
        translate: 0,
        activeIndex: 0,
      });
    }

    setState({
      ...state,
      activeIndex: activeIndex + 1,
      translate: (activeIndex + 1) * width,
    });
  };
  return (
    <>
      <div
        key={`${activeIndex}+CarouselMain`}
        style={{
          display: "inline-block",
          height: `${width/3}px`,
          width: `${width}px`,
          position: "relative",
        }}
      >
        <CarouselItem
          translate={translate}
          transition={transition}
          width={width * slideConfigurations.length}
        >
          {slideConfigurations.map((configuration, i) => {
            return <CarouselImage key={`${i}+CarouselImage`} slideIndex={i} configuration={configuration}></CarouselImage>;
          })}
        </CarouselItem>
        <CarouselArrows direction="right" handleClick={nextSlide} />
        <CarouselArrows direction="left" handleClick={prevSlide} />
        <CarouselDots configurations={slideConfigurations} activeIndex={activeIndex} />
      </div>
    </>
  );
};

export default Carousel;
