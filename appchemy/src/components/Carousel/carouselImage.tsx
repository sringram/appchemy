import React from "react";
import CarouselCaption from "./carouselCaption";
import { useWindowSize } from "../../Providers/ScreenProvider"

const CarouselImage = ({ slideIndex, configuration }) => {
  const {width, height} = useWindowSize()
  return (
    <div
      style={{
        height: `100%`,
        width: `100%`,
        backgroundImage: `url('${configuration.img}')`,
        backgroundSize: `cover`,
        backgroundRepeat: `no-repeat`,
        backgroundPosition: `center`,
        overflow: 'hidden'
      }}
    >
      <CarouselCaption
        key={`${slideIndex}+carouselCaptionBox`}
        titeText={configuration.titeText}
        callout1={configuration.callOut1}
        callout2={configuration.callout2}
        callout3={configuration.callout3}
      />
    </div>
  );
};

export default CarouselImage;
