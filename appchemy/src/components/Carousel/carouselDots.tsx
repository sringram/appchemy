import React, { useState, useEffect } from 'react'
import { useWindowSize } from '../../Providers/ScreenProvider'

const Dot = ({active, cssStyles}) => (
  <span
    style={{
      padding: `${cssStyles.dot.padding}`,
      marginRight: `${cssStyles.dot.marginRight}`,
      cursor: `${cssStyles.dot.cursor}`,
      borderRadius: `${cssStyles.dot.borderRadius}`,
      background: `${active ? 'black' : 'white'}`,
    }}
  />
)

const Dots = (props) => { 
  const { width, height } = useWindowSize();
  const Desktop = {
    name: "DesktopDots",
    dot: {
      padding: '10px',
      marginRight: '5px',
      cursor: 'pointer',
      borderRadius: '50%',
      background: `${props.active ? 'black' : 'white'}`,
    },
    position: 'absolute',
    bottom: '1vw',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
  const Tablet = {
    name: "DesktopDots",
    dot: {
      padding: '5px',
      marginRight: '5px',
      cursor: 'pointer',
      borderRadius: '50%',
      background: `${props.active ? 'black' : 'white'}`,
    },
    position: 'absolute',
    bottom: '1vw',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

  }
  const Mobile = {
    name: "DesktopDots",
    dot: {
      padding: '3px',
      marginRight: '5px',
      cursor: 'pointer',
      borderRadius: '50%',
      background: `${props.active ? 'black' : 'white'}`,
    },
    position: 'absolute',
    bottom: '1vw',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
  let originalState = Desktop
  if(width > 500 && width <= 1000){
    originalState = Tablet

  }
  else if (width <= 500) {
    originalState = Mobile
  }
  const [cssStyles, setCssStyles] = useState(originalState);

  useEffect(() => {
      if(width > 1000){
        setCssStyles(Desktop)
      }
      else if(width > 500 && width <= 1000){
        setCssStyles(Tablet)
      }
      else {
        setCssStyles(Mobile)
      }
  }, [width])
  return(
    <div
      key={`${props.configurations}+D`}
      style={{
        position: `${cssStyles.position}`,
        bottom: `${cssStyles.bottom}`,
        width: `${cssStyles.width}`,
        display: `${cssStyles.display}`,
        alignItems: `${cssStyles.alignItems}`,
        justifyContent: `${cssStyles.justifyContent}`,
      }}
    >
      {props.configurations.map((configuration, i) => (
        <Dot key={`${configuration}+${i}`} active={props.activeIndex === i} cssStyles={cssStyles} />
      ))}
    </div>
  )
}

export default Dots