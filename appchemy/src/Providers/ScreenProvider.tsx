import React, { createContext, useState, useEffect} from "react";
const screenSizeContext = createContext({width: window.innerWidth, height: window.innerHeight});

const ScreenSizeProvider = ({children}) => {
    const [width, setWidth] = useState(window.innerWidth)
    const [height, setHeight] = useState(window.innerHeight)
    useEffect(() => {
        const updateSize = () => {
          setWidth(window.innerWidth);
          setHeight(window.innerHeight)
        }
        window.addEventListener('resize', updateSize)
        updateSize()
        return () => {
          window.removeEventListener('resize', updateSize)
        };
      }, [])
    return(
        <screenSizeContext.Provider value={{ width, height }}>
            {children}
        </screenSizeContext.Provider>
    )
};

export const useWindowSize = () => {
    const { width, height } = React.useContext(screenSizeContext);
    return { width, height };
}

export default ScreenSizeProvider